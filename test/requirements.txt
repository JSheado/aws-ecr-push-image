pytest==4.3.0
flake8==3.7.7
docker==3.7.0
awscli==1.16.174
boto3==1.9.115

bitbucket-pipes-toolkit==1.14.2
