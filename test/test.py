import os
from uuid import uuid4

import boto3

from bitbucket_pipes_toolkit.test import PipeTestCase


class ECRTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        self.base_name = "bbci-pipes-test-ecr"
        self.repository_name = f"{self.base_name}-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
        self.image_name = self.repository_name
        self.tag = uuid4().hex
        self.image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=self.image_name)
        self.image.tag(self.image_name, self.tag)

    def test_default_success(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "REPOSITORY": self.repository_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

    def test_image_is_available_in_ecr(self):
        client = boto3.client('ecr', region_name=os.getenv('AWS_DEFAULT_REGION'))

        tags = f"{uuid4().hex} {uuid4().hex}"

        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "TAGS": tags,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Successfully pushed', result)

        images = client.list_images(repositoryName=self.repository_name)
        ecr_tags = [image.get('imageTag') for image in images['imageIds']]
        self.assertIn(tags.split()[0], ecr_tags)
        self.assertIn(tags.split()[1], ecr_tags)

    def test_missing_variables(self):
        result = self.run_container(environment={
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('AWS_ACCESS_KEY_ID:\n- required field', result)

    def test_image_build_fails_image_doenst_exist(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": "no-such-image",

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Image not found', result)

    def test_image_push_fails_invalid_tag(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "TAGS": 'hello:hi',

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Docker tag error', result)

    def test_debug_messages_are_in_logs(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,
            "DEBUG": 'true',

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('DEBUG:', result)


class ECRMissingRepositoryTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        self.image_name = 'no-such-repository'
        self.tag = uuid4().hex
        super().setUp()
        self.image, _ = self.docker_client.images.build(
            path=os.path.join(os.path.dirname(__file__), 'context'), tag=self.image_name)
        self.image.tag(self.image_name, self.tag)

    def test_image_push_fails_repository_doent_exist(self):
        result = self.run_container(environment={
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "IMAGE_NAME": self.image_name,

            "DOCKER_HOST": "tcp://host.docker.internal:2375"
        }, extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')})

        self.assertIn('Docker push error', result)
