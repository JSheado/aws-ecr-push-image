# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.3

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.2

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.1.0

- minor: Add default values for AWS variables.

## 1.0.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.1

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.0.0

- major: Replaced the TAG with the TAGS parameter to support pushing multiple tags

## 0.2.0

- minor: Added warning messages when a new version of the pipe is available

## 0.1.3

- patch: Documentation improvements to provide more information about docker build and docker layers caching.
- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.2

- patch: Internal release

## 0.1.1

- patch: Bump the pipe patch version

## 0.1.0

- minor: Initial release

